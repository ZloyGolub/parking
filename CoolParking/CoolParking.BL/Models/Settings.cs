﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static int StartBalance { get; }
        public static int ParkingCapacity { get; }
        public static double TimerOut { get; }
        public static double TimerLog { get; }

        public static Dictionary<VehicleType, decimal> Tariffs = new Dictionary<VehicleType, decimal> {
            [VehicleType.PassengerCar] = 2m,
            [VehicleType.Truck] = 5m,
            [VehicleType.Bus] = 3.5m,
            [VehicleType.Motorcycle] = 1m
        };
        public static decimal FineCoefficient { get; set; }
        public static string LogPath { get; }

        static Settings()
        {
            TimerOut = 5;
            TimerLog = 60;
            FineCoefficient = 2.5m;
            ParkingCapacity = 10;
        }
    }
}
