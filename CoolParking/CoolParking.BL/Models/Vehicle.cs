﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [Required]
        public string Id {get; }
        [Required]
        public VehicleType VehicleType { get; }
        [Required]
        [Range(1, (double)decimal.MaxValue)]
        public decimal Balance { get; set; } //TODO The Balance should be able to change only in the CoolParking.BL project.

        public Vehicle(string plate, VehicleType type, decimal money)
        {
            if (IsIdValid(plate) && IsBalanceValid(money))
            {
                this.Id = plate;
                this.VehicleType = type;
                this.Balance = money;
            }
            else throw new ArgumentException();
        }

        private bool IsIdValid(string plate)
        {
            if (Regex.IsMatch(plate, @"^[A-Z]{2}-\d{4}-[A-Z]{2}(?=\r?$)"))
                return true;
            else return false;
        }
        private bool IsBalanceValid(decimal balance)
        {
            if (balance > 0)
            {
                return true;
            }
            else return false;
        }
    }
}