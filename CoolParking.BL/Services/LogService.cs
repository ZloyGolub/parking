﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; set; }

        public string Read()
        {
            string actual;
            using (var file = new StreamReader(LogPath))
            {
                actual = file.ReadToEnd();
            }
            return actual;
        }

        public void Write(string logInfo)
        {
            using (var file = new StreamWriter(LogPath, true))
            {
                file.WriteLine(logInfo);
            }
        }

        public LogService(string path)
        {
            this.LogPath = path;
        }
    }
}